import React from "react";
import { connect } from "react-redux";
import { fetchStream, editStream } from "../../actions";
import StreamForm from "./StreamForm";
import _ from 'lodash'
class StreamEdit extends React.Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }

  onSubmit = (formValues) => {
    console.log("edit onsubmit", this.props)
   this.props.editStream(this.props.stream.id, formValues)
  }
  render() {
    console.log("eidt", this.props);
    if (!this.props.stream) {
      return <div>loading </div>;
    } else {
      return (
        <div>
          <h3>Edit a stream </h3>
          <StreamForm 
          initialValues = {_.pick(this.props.stream, 'title', 'description')} 
          onSubmit={this.onSubmit} />  {/* initialValues, redux form parameter */} 
          
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return { stream: state.streams[ownProps.match.params.id] };
};
export default connect(
  mapStateToProps,
  { fetchStream,editStream }
)(StreamEdit);
