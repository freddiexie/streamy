import React from "react";
import { connect } from "react-redux";
import { fetchStreams } from "../../actions"
import {Link} from 'react-router-dom'

class StreamList extends React.Component {
  componentDidMount() {
    console.log("did mount ------------------------------------");
    this.props.fetchStreams();
  }
  renderList = () => {
    return this.props.streams.map(stream => {
      return (
        <div className="item" key={stream.id}>
          {this.renderAdmin(stream)} {/*put here becuase of  some sematic ui issue*/}
          <i className="large middle aligned icon camera" />
          <div className="content">
            <Link to = {`/streams/${stream.id}`} className="header"> {stream.title}</Link>
            <div className="description">{stream.description}</div>
          </div>
         
        </div>
      );
    });
  };
  renderAdmin = stream => {
    
    if (stream.userId !== undefined && stream.userId === this.props.currentUserId) {
      
      return (
        <div className="right floated content">
        <Link to = { `/streams/edit/${stream.id}`} className="ui button primary"> Edit </Link>     
        <Link to = { `/streams/delete/${stream.id}`} className="ui button negative"> Delete </Link>
        </div>
      );
    }
  };
  renderCreate =()=>{
    if(this.props.isSignedIn){
      return (<div style={{textAlign:"right"}} >
        <Link to ="/streams/new" className='ui button primary'>Create Stream</Link>

      </div>)
    }

  }
  render() {
    console.log("render ", this.props.streams);
    return (
      <div>
        <h2>Stream</h2>
        <div className="ui celled list">{this.renderList()}</div>
        {this.renderCreate()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log("state to prop", Object.values(state.streams));
  return {
    currentUserId: state.auth.userId,
    isSignedIn : state.auth.isSignedIn,
    streams: Object.values(state.streams)
  }; // object.values pulls out values from object and put in to an array
};

export default connect(
  mapStateToProps,
  { fetchStreams }
)(StreamList);
